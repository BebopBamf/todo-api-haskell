{-# LANGUAGE DataKinds #-}
{-# LANGUAGE TypeOperators #-}

module Api.Todo where

import GHC.Int (Int32)

import Control.Monad.Reader (MonadIO (..), when)
import Servant

import Core
import Models

type TodoAPI =
    "todos" :> Get '[JSON] [Todo]
        :<|> "todos" :> Capture "todoId" Int32 :> Get '[JSON] Todo
        :<|> "todos" :> ReqBody '[JSON] TodoForm :> Post '[JSON] Todo

--    :<|> "todos" :> Capture "todoId" Int :> ReqBody '[JSON] Todo :> Put '[JSON] Todo
--    :<|> "todos" :> Capture "todoId" Int :> Delete '[JSON] ()

todoAPI :: Proxy TodoAPI
todoAPI = Proxy

todoServer :: (MonadIO m) => ServerT TodoAPI (AppT m)
todoServer = allTodos :<|> readTodoById :<|> createTodo

allTodos :: (MonadIO m) => AppT m [Todo]
allTodos = selectDB selectTodo

readTodoById :: (MonadIO m) => Int32 -> AppT m Todo
readTodoById id_ = do
    query <- selectDB $ selectTodoById id_
    case query of
        [] -> throwError err404
        (x : _) -> return x

createTodo :: (MonadIO m) => TodoForm -> AppT m Todo
createTodo form = do
    todo_ids <- insertDB $ insertTodo form
    case todo_ids of
        [] -> throwError err500
        (todo_id : _) -> do
            todo' <- selectDB $ selectTodoById todo_id
            case todo' of
                [] -> throwError err500
                (t : _) -> return t
