FROM haskell:9.2.7-slim AS builder

# Install Dependancies

RUN apt-get update && \
    apt-get install -y --no-install-recommends \
    gcc \
    build-essential \
    libpq-dev

# Build Project

WORKDIR /usr/src/workspace

COPY stack.yaml /usr/src/workspace/stack.yaml
COPY package.yaml /usr/src/workspace/package.yaml
RUN stack build --dependencies-only

COPY . /usr/src/workspace/

RUN stack path --local-bin && \
    stack install

FROM debian:bookworm-slim

RUN apt-get update && \
    apt-get install -y --no-install-recommends \
    libpq-dev

WORKDIR /srv/app

COPY --from=builder /root/.local/bin/todo-server /usr/local/bin/todo-server
COPY --from=builder /usr/src/workspace/config.yaml /srv/app/config.yaml
COPY --from=builder /usr/src/workspace/migrations/ /srv/app/migrations/
COPY --from=builder /usr/src/workspace/entrypoint.sh /srv/app/entrypoint.sh

ENTRYPOINT [ "/srv/app/entrypoint.sh" ]

