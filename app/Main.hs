module Main (main) where

import System.Environment (getArgs)

import Init

main :: IO ()
main = do
    args <- getArgs
    case args of
        ["migrate"] -> doMigrations
        _ -> startApp
