CREATE TABLE IF NOT EXISTS todos (
    id serial PRIMARY KEY NOT NULL,
    name varchar (255) NOT NULL,
    description text NOT NULL DEFAULT '',
    is_complete bool NOT NULL DEFAULT FALSE,
    created_at timestamp NOT NULL DEFAULT now(),
    updated_at timestamp NOT NULL DEFAULT now()
);
